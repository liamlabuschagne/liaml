<h1>Rule #1 Investing</h1>
<img src="/img/blog/rule-one-investing/rule-1.webp" alt="Rule #1 by Phil Town">
<p>
    In this article I hope to summarise an investing strategy outlined in a book called "Rule #1" by Phil Town which has
    so far been <b>the best stock market investing book I have ever read</b> due to it's simplicity and practicality, it
    has many interesting stories but here I will only be focusing on the core strategy taught in the book.
</p>
<p>
    <a href="#big-five-calculator">Skip to my "Big Five Calculator"</a>
</p>

<h2>Myths of Investing</h2>
<p>
    The book starts by dispelling some myths about investing which (when I'm honest) I believed to some extent before
    reading the book, things like:
</p>
<ul>
    <li>High returns require high risk</li>
    <li>Mutual funds are less risky</li>
    <li>You have to be an expert to pick stocks</li>
    <li>You can't beat the market</li>
    <li>To reduce risk you must diversify and hold for the long term</li>
    <li>Dollar cost averaging reduces risk</li>
</ul>
<h3>Dispelling the myths</h3>
<p>
    The book goes into great detail dispelling each of these myths but the main points are as follows:
</p>
<ul>
    <li>You only need to spend about 15 minutes a week to achieve great results</li>
    <li>Beating the market is easy if you buy wonderful businesses at a great price (usually a 50% discount)</li>
    <li>By doing this, diversification is not necessary, your risk is reduced because you are buying these business at a
        huge discount (it doesn't have to perform extremely well, it just needs to return to intrinsic value).</li>
</ul>
<h2>What is Rule #1?</h2>
<img src="/img/blog/rule-one-investing/rule-1-warren-quote.webp" alt="Rule #1 Warren Buffet Quote">
<p>
    The core idea of the book as the title suggests is called "Rule #1" which is actually one of Warren Buffet's ideas
    (the worlds best investor) which is as follows:
</p>
<ol>
    <li>Don't lose money</li>
    <li>Don't forget rule #1.</li>
</ol>
<p>
    This is to say, being <b>a good investor requires exactly the same skills as being a rational consumer</b>. If you
    have ever spent a few hours researching a product before buying it, comparing offerings from different companies and
    making an informed decision based on that, you already know most of what you need to start investing. It is the
    exact same process.
</p>
<h2>The Four M's</h2>
<p>
    The rule #1 investing strategy revolves around <b>buying a wonderful business at an attractive price</b>, if you can
    answer yes to these two questions (is it a wonderful business and is it at an attractive price?), you can
    confidently invest in the business. To do this we use the 4 M's checklist as follows:
</p>
<ol>
    <li>
        <b>Meaning</b>: Does the business have meaning to you, do you understand it and does it align with your values?
    </li>
    <li><b>Moat</b>: Does the business have some sort of competitive advantage that makes it hard for competitors to
        "storm the castle"?</li>
    <li><b>Management</b>: Are the people running the business in it for the short-term returns on their stock options
        and
        getting huge bonuses or do they truly care about the long-term success of the business?</li>
    <li> <b>Margin of Safety</b>: Is the business being offered to you at a steep discount to it's sticker price
        (hopefully 50% or more).</li>
</ol>
<h2>You own a business, not a stock</h2>
<p>
    Another key principal that the book teaches is the idea that when you invest on the stock market, you should be
    thinking in terms of buying a part of a business not shares of a stock. This makes you treat every
    investment decision as just that - an investment rather than a speculative bet (this is also part of the reason it
    is not as risky as make it out to be). Phil Town sums this up in a rule he calls the 10-10 rule which says, <b>you
        shouldn't own a stock for 10 seconds if you wouldn't be proud to own that stock for 10 years</b>.
</p>
<h3>The Punch Card Analogy</h3>
<p>
    A complementary idea to this is Warren Buffet's punch card analogy: Imagine that everyone in the world is born with
    a punch card that can only ever get a total of 20 holes punched into it, every time you make a bet on a business you
    use one of your 20, so each one must be carefully thought through.
</p>
<h2>Margin of Safety</h2>
<img src="/img/blog/rule-one-investing/margin-of-safety.webp" alt="Margin of Safety">
<p>
    To know if a business is being offered at a healthy margin of safety it is natural to ask, how do I know what the
    value of the business is? Phil Town calls this the sticker price. The way we do this is by first putting the
    company's financials through the ringer - do they actually make money and return this as value to shareholders or
    are they burning more and more cash every year? This is important because <b>we can't pay an infinite price for even
        the best business </b> otherwise there is no upside.
</p>
<h3>The Big Five</h3>
<p>
    The Big Five are the top five most important metrics we need in order to evaluate a business's financials. The
    important thing to note here is that we should expect to have <b>a minimum of 15% return per year</b> which is a
    healthy margin above the market average (i.e index funds) especially when you factor in losses for inflation.
</p>

<h4>What we are looking for</h4>
<p>
    For each of these metrics we must evaluate the 10 year, 5
    year and 1 year compounded growth rates to see if there is any significant trend (like slowing/accelerating growth)
    These include:
</p>
<ol>
    <li><b>ROIC (return on invested capital)</b>: This is a percentage value which describes what return the management
        are achieving for every spare dollar they re-invest into their own business. If they are achieving less than 10%
        in this category it means they are investing your money less effectively than what a broad market index fund
        could be doing (especially when accounting for inflation). If this is negative, it is like throwing money into a
        black hole - you will likely never see that money again.
    </li>
    <li><b>Sales (Revenue)</b>: This is often refereed to as "top line growth" as it is the top line in an income
        statement. This is the total money coming into the business before any expenses are paid. You want this number
        to also be increasing by 10% a year because if it isn't, how will the cash left over at the end of the year (the
        value being delivered to you as a shareholder) possibly be growing at that rate or higher?
    </li>
    <li><b>EPS (earnings per share) Growth</b>: This is how much the total earnings of the business (sort of like final
        profit after all expenses) is growing from year to year represented on a per share basis. Again we want this to
        be growing at a rate of at least 10% per year.
    </li>
    <li><b>Equity Growth</b>: The difference between the business' assets (what the company owns)
        and liabilities (what the companies owes). Assets include any money the company has in the bank, property,
        equipment, investments etc. Liabilities include any debt the company has which can range from short to long
        term.
    </li>
    <li><b>FCF (free cash flow) Growth</b>: This is the money left over after all expenses and capital expenditure
        (money invested back into the business). It will end up in a company's bank account at
        the end of the year or it can be distributed to shareholders in the form of a dividend. This too should be
        growing every year but an important thing to note is that along with this growing, we want to see a growing
        amount invested back into the business (refer to ROIC).
    </li>
</ol>
<h4>How do I calculate these?</h4>
<p>
    Simple! Just look up the company's investor relation page and trawl through years and years of
    financial statements, putting them into a complicated spreadsheet with growth formulas... Just kidding!
</p>
<p>
    There is a really incredible FREE website called <a target="_blank" href="https://quickfs.net">QuickFS</a> which
    gives you up to 10 years of financial data, I would strongly recommend creating an account so that you can access
    the FCF figures as well as historical PE ratios which you will need later.</p>
<p>
    On this website you will be able to access all of the metrics I've mentioned. For all of them except for ROIC you
    can use <a target="_blank" href="https://www.ruleoneinvesting.com/equity-growth-rate-calculator/">Phil Town's
        own growth calculator</a> (don't worry about the fact this is the equity one as they are all the same)
    which will give you the compounded growth rate required to get from the starting value to the ending value over the
    time span specified.
</p>
<p>
    <b>For example:</b> for the 10 year growth rate you want to enter the starting value (10 years ago) and the current
    year's value with a time span of 9 years between them.
</p>
<p>
    <b>Important</b>: For ROIC, calculate the average (not a growth rate) for the last 10 years and 5 years. For the 1
    year result it is just the number QuickFS shows you for the last year.
</p>
<div id="big-five-calculator">
    <h3>The Big Five Calculator</h3>
    <p>If you are logged in to QuickFS.net <a target="_blank" href="https://quickfs.net/home">get your api
            key</a> and enter it below to calculate the big five automagically!</p>
    <p><b>Are all the numbers above 10%? Are they growing over time (left to right)?</b></p>
    <label for="apikey">QuickFS API Key:</label>
    <input type="text"><br>
    <label for="ticker">Ticker (e.g ATM:NZ):</label>
    <input type="text">
    <button>Calculate</button>
    <p style="color: red;"></p>
    <table>
        <tr>
            <th>Metric</th>
            <th>10 Year</th>
            <th>5 Year</th>
            <th>1 Year</th>
        </tr>
        <tr id="revenue">
            <td>Revenue</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="eps_diluted">
            <td>EPS</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="total_equity">
            <td>Equity</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="fcf">
            <td>FCF</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="roic">
            <td>ROIC</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
<br>
<h4>A quick debt check...</h4>
<p>
    Even though debt is not one of the big five it is worth checking if the company can at least <b>pay off it's long
        term debt within 3 years</b>. To do this, find last year's FCF and long-term debt values
    in the "key ratios" and "balance sheet" pages of <a target="_blank" href="https://quickfs.net">QuickFS</a>
    and divide long-term debt into free cash flow, if the number
    is more than 3, it will take longer than 3 years to pay it off - not good. If this is the case, pass on the
    business, you don't want even the slightest chance of there being major debt problems in the future - especially if
    a recession happens.
</p>

<h3>Summary of The Big Five</h2>
    <p>If your company passes all these tests (every value is above 10%) then you can move on to the evaluation
        stage - find out how much you should pay. An important thing to note for this section is that if even one number
        is marginal or dodgy looking, pass on this investment. You want it to be so obvious the numbers are good that
        you even if you make a mistake in your calculations it should still work out. <b>You should be actively looking
            for ways to rule a business out</b>.
    </p>

    <h2>Calculating the Sticker Price</h2>

    <p>
        <a target="_blank" href="https://www.ruleoneinvesting.com/margin-of-safety-calculator/">Phil town has a great
            calculator on his
            website for this</a> once again, I would recommend you just use that instead
        of doing these calculations yourself. You will need 3 numbers:
    </p>

    <img src="/img/blog/rule-one-investing/margin-of-safety-calculator.webp"
        alt="Phil Town Margin of Safety Calculator">

    <ul>
        <li>The <b>EPS (TTM)</b> of the company (find this on the the summary page of the company's listing on <a
                target="_blank" href="https://nz.finance.yahoo.com">Yahoo!
                Finance</a>)
        </li>
        <li>
            Your <b>estimated growth rate</b> which should be the more conservative option between the 10 year equity
            growth rate you calculated for The Big Five and the analysts' 5 year growth rate available on Yahoo! Finance
            at the bottom of the "analysis" page of a stock's listing.
        </li>
        <li>
            The <b>future PE (price/earnings) ratio</b> which should again be the more conservative option between:
            <ul>
                <li>the historical average 10 year PE ratio (bottom of "key rations" page in QuickFS)</li>
                and
                <li>2x growth rate you
                    calculated (e.g if the growth rate is 12% then the future PE is 24)</li>
            </ul>
        </li>
    </ul>

    <p>
        Once you have these numbers, put them in the calculator which will give you the current sticker price (intrinsic
        value) and a 50% margin of safety price (<b>the price you want to pay at a maximum</b>).</p>
    <h3>Once you have your MOS price:</h3>
    <p>Compare the margin of
        safety price to the current stock price (again available on Yahoo! Finance). If the current stock price is at or
        below you margin of safety price...</p>
    <p>
        <b>JUMP OUT OF YOUR SEAT IN EXCITEMENT!!!</b>
    </p>
    <p>
        If not, put this company on your watchlist and wait until it is. It's really that easy!
    </p>

    <h2>How to get in...</h2>
    <p>
        Now that we have the price we want to get in at, we just need to wait for the momentum of the stock to start
        heading in the right direction so we know it has reached a local minimum. This is important because <b>a stock
            may end up plummeting well below our margin of safety price </b> (Hooray!).
    </p>
    <p>If we buy too early it may
        take much longer for us to start getting a return as well as the gains we are forgoing by not starting at rock
        bottom prices. The market, in the long run will always price a business back to it's intrinsic value.
    </p>
    <h3>The "big guys" are the market</h3>
    <p>
        <b>The market is mostly controlled by big institutional investors </b> who can take weeks to get into and out of
        stocks. This is because they are buying vastly larger quantities of stock than you or I so by the time they have
        finished buying their $20 million worth of shares the stock price could have increased significantly. We don't
        have this problem and we can use it to our advantage. All we have to do is <b>wait for the big guys to start
            getting in and then just ride the wave</b>.
    </p>
    <h3>Technical Indicators</h3>
    <img src="/img/blog/rule-one-investing/indicators.webp" alt="Stock Market Technical Indicators">
    <p>
        I have always been sceptical of technical indicators since I had always associated them with highly speculative
        day-trading but if you pick just a few simple ones to help you identify momentum in the market it can be a great
        confidence booster when going into a trade.
    </p>
    <p>
        The three indicators you need are: 10 day moving average, the MACD (8,17,9) and the Stochastics (14 day). These
        can all be applied on the free charting tool in Yahoo! Finance. Simply look up your stock and click on the
        chart tab, then add the three indicators with the settings I have mentioned and then you want to look for the
        following:
    </p>
    <ul>
        <li><b>Moving Average</b> - This moving average smooths out the volatility of a stock a gives you an insight
            into the
            general trend of a stock. When the current stock price line jumps up above this, Phil Town explains that
            this is <b>like breaking a psychological barrier</b> meaning that investors have generally had
            a change in attitude toward the stock.<br><br> You can confirm this by sliding back a bit on the chart and
            noticing
            how for the vast majority of the time after the stock price breaks through the moving average, the moving
            average generally follows that direction (either up or down). If it breaks through above it, this is a "buy
            signal" but that is only one indicator's opinion...
        </li>
        <li>
            The second is the <b>MACD (moving average convergence/divergence)</b> which is sort of like measuring the
            "pressure in a hose", if this is on an upward trend it means buying pressure is increasing and a downward
            trend means selling pressure is increasing. You will see that it oscillates between making a mountain or a
            valley, when it looks as though a new mountain is being formed, this is the "buy signal" for this indicator.
        </li>
        <li>
            The third indicator is the <b>Stochastics</b> which gives us an idea of the overbuying or overselling of a
            stock in a percentage term. If it is above 80% it is being highly overbought and below 20% it is being
            highly oversold. When the black line ("buy line") moves above the red line ("sell line") it
            suggests an increase in upward buying pressure, if this is recovering from an overselling position (20% or
            below) this is a "buy signal" for this stock.
        </li>
    </ul>
    <h4>Summary of Indicators</h4>
    <p>
        So basically when all three indicators are screaming "BUY!" it likely means the "big guys" (the institutional
        investors who ARE the market) are getting in and it's time to ride the wave. The opposite is true when they all
        scream "SELL!".<b>Remember that we are not day trading</b> - this is simply our final indication of when to buy
        once we have confirmed this is a wonderful business at an attractive margin of safety. In other words we are
        just waiting for the momentum to start heading in the right direction.
    </p>

    <h2>Conclusions</h2>
    <p>
        So if you got though all that congratulations! I know it's a lot of reading but you will soon find most of this
        information becomes clear the more time you spend researching businesses and making low-risk and
        infrequent bets.<b>You only need to find one or two good investment ideas per year</b> that meet the 4 M's and
        the rest of the time you just lightly monitor your stocks (about 15 minutes a week). Notice that in this case
        having less stocks (less diversification) is actually less risky because there is less to monitor!
    </p>
    <h2>How do I start?</h2>
    <img src="/img/blog/rule-one-investing/how-do-i-start-investing.webp"
        alt="How do I start investing in the stock market?">
    <p>
        Thanks for reading this article and I genuinely hope it helps you to become a great part-time or even full-time
        investor which should hopefully allow you more financial freedom because you aren't reliant on a 9-5 job to stay
        afloat.
    </p>
    <p>If you would like to start investing real money (after some practise using a paper-trading account which
        is like a risk-free simulator) you can use any of the links below to get a small amount of extra <b>FREE
            money</b> to help start your investing journey (and you'll support me since I get the same!):
    </p>
    <p>
        If you are unsure as to which platform to use for your style of investing, check out my earlier blog post <a
            href="http://liaml.local/blog/nz-investing">comparing all the
            major NZ investing platforms</a>.
    </p>
    <ul>
        <li>Sharesies: <a target="_blank" rel="noopener"
                href="https://sharesies.nz/r/PPR7D3">https://sharesies.nz/r/PPR7D3</a>
            (we both get $5)</li>
        <li>Stake: <a target="_blank" rel="noopener"
                href="https://hellostake.com/referral-program?referrer=liaml539">https://hellostake.com/referral-program?referrer=liaml539</a>
            (we both get a free stock)
        </li>
        <li>Hatch: <a href="https://app.hatchinvest.nz/share/xsexnpbk" target="_blank"
                rel="noopener">https://app.hatchinvest.nz/share/xsexnpbk</a> (we both get a $10 NZD top up when you
            deposit
            your first $100).
        </li>
    </ul>