<?php
function upload($target_dir, $name)
{
    $target_file = $target_dir . $name;
    $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION));
    $target_file .= "." . $imageFileType;

    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check == false) {
            echo "Not an image.";
            return false;
        }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        echo "File Exists.";
        return false;
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 5000000) {
        echo "File to large. (Max 500KB)";
        return false;
    }

    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        echo "Only jpg and png allowed";
        return false;
    }

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        return $target_file;
    } else {
        echo "File upload failed.";
        return false;
    }
}

function resize_and_convert($target_file, $max_width)
{
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $newFile = str_replace($imageFileType, "", $target_file) . "webp";

    // Get Image info
    $imginfo = getimagesize($target_file);
    $width = $imginfo[0];
    $mimetype = $imginfo["mime"];

    // Convert image to object by mime type
    if ($mimetype == "image/png") {
        $img = imagecreatefrompng($target_file);
        imagepalettetotruecolor($img);
        imagealphablending($img, true);
        imagesavealpha($img, true);
    } else if ($mimetype == "image/jpeg") {
        $img = imagecreatefromjpeg($target_file);
    }

    // Check if scaling is required
    if ($width > $max_width) {
        $img = imagescale($img, $max_width);
    }

    // Covert and save
    imagewebp($img, $newFile, 90);
    // Cleanup
    imagedestroy($img);
    return str_replace("..", "", $newFile);
}

if (isset($_POST['submit'], $_FILES["fileToUpload"]) && $_FILES["fileToUpload"]['size'] > 0) {
    $target_file = upload("../img/blog/", $_POST['name']);
    if ($target_file) {
        $newFile = resize_and_convert($target_file, 800);
        if ($newFile) {
            echo "<a href='$newFile'>$newFile</a>";
        }
    }
}

?>
<form action="" method="post" enctype="multipart/form-data">
  Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload"><br>
  /img/blog/<input type="text" name="name">.webp<br>
  <input type="submit" value="Upload Image" name="submit">
</form>
