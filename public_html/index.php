<?php
$uri = $_SERVER['REQUEST_URI'];

if (strpos($uri, "?") !== false) {
    $uri = substr($uri, 0, strpos($uri, "?"));
}

$uri = explode("/", $uri);

if ($uri[count($uri) - 1] == "") {
    array_pop($uri);
}

function getPage()
{
    global $uri, $page, $base_css_path;
    $page = "";
    foreach ($uri as $key => $seg) {
        if ($key == count($uri) - 1) {
            $base_css_path = "/css" . $page . "base.css";
            $page .= $seg;
        } else {
            $page .= $seg . "/";
        }
    }
}
getPage();

function check_page($page)
{
    return file_exists("./html/" . $page . ".html");
}

while (!check_page($page)) {
    $page .= "/index";
    if (check_page($page)) {
        break;
    }
    substr($page, 0, strlen($page) - 5);
    array_pop($uri);
    getPage();
}

$html_path = "/html" . $page . ".html";
$css_path = "/css" . $page . ".css";
$js_path = "/js" . $page . ".js";

$pages = [
    "/" => ["Liam Labuschagne", "This is the personal website of Liam Labuschagne."],
    "/blog" => ["Blog", "This is personal blog of Liam Labuschagne."],
    "/videos" => ["Videos", "This is a collection of videos created by Liam Labuschagne."],
    "/games" => ["Games", "This is a collection of HTML5 web games made by Liam Labuschagne."],
    "/apps" => ["Apps", "This is a collection of desktop and mobile apps created by Liam Labuschagne"],
    "/blog/abortion-is-murder" => ["Abortion is Murder", "This is a formal pro-life argument by Liam Labuschagne."],
    "/blog/rewardia-hack" => ["Rewardia Hack", "This is a hack I made for the rewardia.co.nz website which is able to earn real money."],
    "/blog/nzherald-hack" => ["NZ Herald Hack", "This is a hack I made which allows you to bypass the Premium subscription on the NZ Herald"],
    "/blog/nz-investing" => ["NZ Investing Platforms Compared", "A comparison of all the major investing platforms in New Zealand where I find out which
    one is best in most scenarios."],
    "/blog/rule-one-investing" => ["Rule #1 Investing", "This my summary of Phil Town's book 'Rule #1' which is the best stock market investing book I have read to date."],
    "/blog/investing-plan" => ["Investing Plan", "This post is a list of 7 rules I have come up with to guide my month-to-month spending, investing and saving."],
    "/blog/best-savings-bank-accounts-nz" => ["Best Savings Bank Accounts NZ", "This is a very comprehensive list of all the savings bank accounts in new zealand with a handy sort feature."],
];

if ($_SERVER['REQUEST_URI'] == "/sitemap.xml") {
    header("Content-Type: application/xml");
    $sitemapText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
    foreach ($pages as $uri => $value) {
        $sitemapText .= "\n<url>\n<loc>https://liaml.tk$uri</loc></url>";
    }
    $sitemapText .= "\n</urlset>";
    echo $sitemapText;
    exit;
}
if (substr($page, strlen($page) - 6, strlen($page) - 1) == "/index") {
    $page = substr($page, 0, strlen($page) - 6);
}

$title = $pages['/'][0];
$description = $pages['/'][1];

if (isset($pages[$page])) {
    $title = $pages[$page][0];
    $description = $pages[$page][1];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <link rel="stylesheet" href="/css/base.css">

    <!-- Google Ads -->
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6276749764841127"
     crossorigin="anonymous"></script>
    <!-- End of Google Ads -->
    <?php
if (file_exists("." . $base_css_path)) {
    ?>
        <link rel="stylesheet" href="<?php echo $base_css_path; ?>">
        <?php
}
?>
    <?php
if (file_exists("." . $css_path)) {
    ?>
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
}
?>
</head>

<body>
    <header>
        <img id="small-logo" src="/img/small-logo.svg" alt="Liam Labuschagne Small Logo">
        <nav>
            <a id="logo" href="/">
                <img src="/img/large-logo.svg" alt="Liam Labuschagne Logo">
            </a>
            <a href="/blog">BLOG<span></span></a>
            <a href="/videos">VIDEOS<span></span></a>
            <a href="/games">GAMES<span></span></a>
            <a href="/apps">APPS<span></span></a>
            <a target="_blank" rel="noopener" href="https://gitlab.com/liamlabuschagne"><img id="gitlab-logo" src="/img/gitlab-logo.webp"
                    alt="Liam Labuschagne's Gitlab Page"></a>
        </nav>
        <img id="burger" src="/img/menu.svg" alt="Burger Menu">
        <img id="close" src="/img/close.svg" alt="Close Menu">
    </header>
    <main>
    <?php
if (file_exists("." . $html_path)) {
    include "." . $html_path;
} else {
    echo "file doesn't exist";
}
?>
    </main>
    <footer>
        <p>Created by Liam Labuschagne &copy; 2020</p>
    </footer>
<script src="/js/base.js"></script>

    <?php
if (file_exists("." . $js_path)) {
    ?>
        <script src="<?php echo $js_path; ?>"></script>
        <?php
}
?>
</body>

</html>