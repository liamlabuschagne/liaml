
let apikey = "";
let ticker = "ATM:NZ";

function cagr(initial, final, period) {
    return Math.round((Math.pow((final / initial), (1 / period)) - 1) * 100);
}

function avg(data, period) {
    let total = 0;
    let text = "";
    for (let i = 1; i <= period; i++) {
        text += data[data.length - i] + "+";
        total += data[data.length - i];
    }
    return total / period;
}

function getDataPoint(data, which, isAvg, perc, period, col) {
    if (data.length >= period) {
        if (isAvg) {
            document.querySelectorAll(`#${which} td`)[col].textContent = Math.round(avg(data, period) * (perc ? 100 : 1)) + "%";
        } else {
            document.querySelectorAll(`#${which} td`)[col].textContent = cagr(data[data.length - period], data[data.length - 1], period - 1) + "%";
        }
    }
}

function getBigFive(which, isAvg, perc) {
    let xhr = new XMLHttpRequest();
    xhr.onloadend = (res) => {
        let data = JSON.parse(res.target.response);

        if (data.error) {
            document.querySelectorAll("p")[1].textContent = "You probably mistyped your api key...";
            return;
        }

        if (data.errors && data.errors.message) {
            document.querySelectorAll("p")[1].textContent = data.errors.message;
            return;
        }

        data = data.data;

        getDataPoint(data, which, isAvg, perc, 10, 1);
        getDataPoint(data, which, isAvg, perc, 5, 2);
        if (which == "roic") {
            getDataPoint(data, which, isAvg, perc, 1, 3);
        } else {
            getDataPoint(data, which, isAvg, perc, 2, 3);
        }
    }
    xhr.open("GET", `https://public-api.quickfs.net/v1/data/${ticker}/${which}?api_key=${apikey}`);
    xhr.send();
}

document.querySelector("button").addEventListener("click", () => {
    document.querySelectorAll("p")[1].textContent = "";
    apikey = document.querySelectorAll("input")[0].value;
    ticker = document.querySelectorAll("input")[1].value;
    getBigFive("roic", true, true);
    getBigFive("revenue", false, true);
    getBigFive("eps_diluted", false, true);
    getBigFive("total_equity", false, true);
    getBigFive("fcf", false, true);
})