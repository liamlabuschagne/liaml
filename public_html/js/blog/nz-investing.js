function applyReduction(perc, amount) {
    return (1 - (perc / 100)) * amount;
}

function applySharesiesBrokerage(amount) {
    if (amount < 3000) {
        return applyReduction(0.5, amount);
    } else {
        return applyReduction(0.5, 3000) + applyReduction(0.1, amount - 3000);
    }
}

function applySharesiesFX(country, amount) {
    if (country != "NZ") return applyReduction(0.4, amount);
    return amount;
}

function sharesies(amount, country, numShares, isETF, numTransactions) {
    amount = Number.parseFloat(amount);
    const original = amount;

    amount = applySharesiesFX(country, amount); // FX in

    for (let i = 0; i < numTransactions; i++) {
        amount = applySharesiesBrokerage(amount); // Brokerage in
        amount = applySharesiesBrokerage(amount); // Brokerage out
    }

    amount = applySharesiesFX(country, amount); // FX out

    let cost = original - amount;

    return "Sharesies: $" + cost.toFixed(5);
}

function investnow(amount, country, numShares, isETF, numTransactions) {
    if (country == 'NZ' && isETF) return "<br>InvestNow: $0.00";
    return "";
}

function applyHatchFX(amount) {
    return applyReduction(0.5, amount);
}

function applyHatchBrokerage(amount, numShares) {
    if (numShares < 300) return amount - 3;
    else return amount - (3 + (numShares - 300) * 0.01);
}

function hatch(amount, country, numShares, isETF, numTransactions) {
    if (country != "US") return "";
    amount = Number.parseFloat(amount);
    let original = amount;

    amount = applyHatchFX(amount); // FX in

    for (let i = 0; i < numTransactions; i++) {
        amount = applyHatchBrokerage(amount, numShares); // Brokerage in
        amount = applyHatchBrokerage(amount, numShares); // Brokerage out
    }

    amount = applyHatchFX(amount); // FX out

    let cost = original - amount;

    return "<br>Hatch: $" + cost.toFixed(5);
}

function applyStakeFX(amount) {
    if (amount * (1 / 100) < 2) return amount - 2;
    else return applyReduction(1, amount);
}

function applyStakeBrokerageBuy(amount, numShares) {
    let brokerage = 0.000119 * numShares;
    if (brokerage > 5.95) {
        brokerage = 5.95;
    }
    return amount - brokerage; // TAF Fee
}

function applyStakeBrokerageSell(amount, numShares) {
    amount -= (0.051 / 10000) * amount; // SEC Fee
    return applyStakeBrokerageBuy(amount, numShares); // TAF fee
}

function stake(amount, country, numShares, isETF, numTransactions) {
    if (country != "US") return "";
    amount = Number.parseFloat(amount);
    let original = amount;

    amount = applyStakeFX(amount); // FX in

    for (let i = 0; i < numTransactions; i++) {
        amount = applyStakeBrokerageBuy(amount, numShares);
        amount = applyStakeBrokerageSell(amount, numShares);
    }

    amount = applyStakeFX(amount); // FX out

    amount -= 2; // Withdrawal fee

    let cost = original - amount;

    return "<br>Stake: $" + cost.toFixed(5);
}

function applyASBFX(country, amount) {
    if (country == "NZ") return amount;
    if (amount * (1 / 100) < 10) return amount - 10;
    else return applyReduction(1, amount);
}

function applyASBBrokerage(country, amount) {
    if (country == "NZ") {
        if (amount <= 1000) return amount - 15;
        if (amount <= 10000) return amount - 30;
        return applyReduction(0.3, amount);
    }
    if (amount * (0.3 / 100) < 30) return amount - 30;
    return applyReduction(0.3, amount);
}

function asb(amount, country, numShares, isETF, numTransactions) {
    if (country == "US") return "";
    amount = Number.parseFloat(amount);
    let original = amount;

    amount = applyASBFX(country, amount); // FX in

    for (let i = 0; i < numTransactions; i++) {
        amount = applyASBBrokerage(country, amount);
        amount = applyASBBrokerage(country, amount);
    }

    amount = applyASBFX(country, amount); // FX out

    let cost = original - amount;

    return "<br>ASB Securities: $" + cost.toFixed(5);
}

function applyJardenFX(country, amount) {
    if (country == "NZ") return amount;
    return applyReduction(1.5, amount);
}

function applyJardenBrokerage(country, amount) {
    if (country == "NZ") {
        if (amount <= 15000) return amount - 29.90;
        amount = 15000 + applyReduction(0.2, amount - 15000);
        amount -= 29.90;
        return amount;
    }
    if (amount <= 30000) return amount - 29;
    amount = 30000 + applyReduction(0.3, amount - 30000);
    amount -= 29;
    return amount;
}

function jarden(amount, country, numShares, isETF, numTransactions) {
    if (country == "US") return "";
    amount = Number.parseFloat(amount);
    let original = amount;

    amount = applyJardenFX(country, amount); // FX in


    for (let i = 0; i < numTransactions; i++) {
        amount = applyJardenBrokerage(country, amount);
        amount = applyJardenBrokerage(country, amount);
    }

    amount = applyJardenFX(country, amount); // FX out


    let cost = original - amount;

    return "<br>Jarden Direct: $" + cost.toFixed(5);
}

function applyIBFX(amount) {
    let feeToRemove = (0.002 / 100) * amount;
    if (feeToRemove < 2) feeToRemove = 2;
    return amount - feeToRemove;
}

function applyIBBrokerage(country, numShares, amount) {
    let feeToRemove = 0;
    if (country == "US") {
        feeToRemove = numShares * 0.0035;
        if (feeToRemove < 0.35) feeToRemove = 0.35;
        if (feeToRemove > (1 / 100) * amount) feeToRemove = (1 / 100) * amount;
    } else if (country == "AU") {
        feeToRemove = amount * (0.08 / 100);
        if (feeToRemove < 5) feeToRemove = 5;
    }

    return amount - feeToRemove;
}

function IB(amount, country, numShares, isETF, numTransactions) {
    if (country == "NZ") return "";
    amount = Number.parseFloat(amount);
    let original = amount;

    amount = applyIBFX(amount); // FX in


    for (let i = 0; i < numTransactions; i++) {
        amount = applyIBBrokerage(country, numShares, amount);
        amount = applyIBBrokerage(country, numShares, amount);
    }

    amount = applyIBFX(amount); // FX out


    let cost = original - amount;

    return "<br>Interactive Brokers: $" + cost.toFixed(5);
}

function hideStuff() {
    let country = document.querySelector('#country').value;
    if (country == 'US') {
        document.querySelector("#numShares").style.display = "block";
    }
    else {
        document.querySelector("#numShares").style.display = "none";
    }

    if (country == "NZ") {
        document.querySelector("#isETF").style.display = "block";
    }
    else {
        document.querySelector("#isETF").style.display = "none";
    }
}

document.querySelector('#country').addEventListener("change", () => {
    hideStuff();
})

hideStuff();

document.querySelector('#go').addEventListener('click', () => {
    let amount = document.querySelector('#amount').value;
    let country = document.querySelector('#country').value;
    let numShares = document.querySelector("#numShares input").value;
    let isETF = document.querySelector('#isETF input').checked;
    let numTransactions = document.querySelector("#numTransactions input").value;

    document.querySelector("#cost").innerHTML = sharesies(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += investnow(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += hatch(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += stake(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += asb(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += jarden(amount, country, numShares, isETF, numTransactions);
    document.querySelector("#cost").innerHTML += IB(amount, country, numShares, isETF, numTransactions);
});