let accounts = [];
function setAccounts() {
    accounts = [
        {
            "institution": "ANZ",
            "name": "Serious Saver",
            "base": "0.1",
            "bonus": "0.45",
            "notes": "No withdrawals and $20 deposit to get bonus"
        },
        {
            "institution": "ANZ",
            "name": "Online",
            "base": "0.15",
            "bonus": "0.15",
            "notes": ""
        },
        {
            "institution": "ASB",
            "name": "On Call",
            "base": "0.25",
            "bonus": "0.25",
            "notes": ""
        },
        {
            "institution": "ASB",
            "name": "Savings Plus",
            "base": "0.05",
            "bonus": "0.4",
            "notes": "no more than 1 withdrawal per month to get bonus"
        },
        {
            "institution": "ASB",
            "name": "Fast Saver",
            "base": "0.15",
            "bonus": "0.15",
            "notes": ""
        },
        {
            "institution": "Bank of Baroda",
            "name": "Savings",
            "base": "0.6",
            "bonus": "0.6",
            "notes": "Minimum balance $500"
        },
        {
            "institution": "Bank of China",
            "name": "Savings",
            "base": "0.1",
            "bonus": "0.1",
            "notes": ""
        },
        {
            "institution": "Bank of India",
            "name": "Star Savings",
            "base": "0.1",
            "bonus": "0.1",
            "notes": "Minimum balance $500"
        },
        {
            "institution": "BNZ",
            "name": "RapidSave",
            "base": "0.45",
            "bonus": "0.45",
            "notes": "1 Free withdrawal per month (then $3)"
        },
        {
            "institution": "ASB",
            "name": "Fast Saver",
            "base": "0.1",
            "bonus": "0.1",
            "notes": ""
        },
        {
            "institution": "China Construction Bank",
            "name": "Savings",
            "base": "0.25",
            "bonus": "0.25",
            "notes": ""
        },
        {
            "institution": "Heartland Bank",
            "name": "Direct Call",
            "base": "0.65",
            "bonus": "0.65",
            "notes": ""
        },
        {
            "institution": "Heartland Bank",
            "name": "YouChoose",
            "base": "0.5",
            "bonus": "0.5",
            "notes": ""
        },
        {
            "institution": "Heartland Bank",
            "name": "Notice Saver",
            "base": "1.4",
            "bonus": "1.4",
            "notes": "Must give 32 days notice"
        },
        {
            "institution": "Industrial and Commercial Bank of China",
            "name": "Savings",
            "base": "0.05",
            "bonus": "0.05",
            "notes": "$1 monthly fee"
        },
        {
            "institution": "Industrial and Commercial Bank of China",
            "name": "Smart Saver",
            "base": "0.6",
            "bonus": "0.6",
            "notes": "No withdrawals allowed, otherwise no interest paid"
        },
        {
            "institution": "Kiwibank",
            "name": "Fast Forward Saver",
            "base": "0.1",
            "bonus": "0.3",
            "notes": "No withdrawals and $20 deposit to get bonus"
        },
        {
            "institution": "Kiwibank",
            "name": "Back-up Saver",
            "base": "0.1",
            "bonus": "0.1",
            "notes": "$1 withdrawal fee"
        },
        {
            "institution": "Kiwibank",
            "name": "Online Call",
            "base": "0.2",
            "bonus": "0.4",
            "notes": "Minimum balance of $2000 for base rate and no withdrawals allowed for bonus"
        },
        {
            "institution": "Kiwibank",
            "name": "First Saver",
            "base": "0.3",
            "bonus": "0.3",
            "notes": "Must be under 19"
        },
        {
            "institution": "Kiwibank",
            "name": "Notice Saver",
            "base": "1.5",
            "bonus": "1.5",
            "notes": "Minimum investment $2000, must give 90 days notice"
        },
        {
            "institution": "Kiwibank",
            "name": "Notice Saver",
            "base": "0.7",
            "bonus": "0.7",
            "notes": "Minimum investment $2000, must give 32 days notice"
        },
        {
            "institution": "Rabobank",
            "name": "PremiumSaver",
            "base": "0.35",
            "bonus": "1.15",
            "notes": "Increase balance by $50 to get bonus"
        },
        {
            "institution": "Rabobank",
            "name": "RaboSaver/PurposeSaver",
            "base": "0.65",
            "bonus": "0.65",
            "notes": ""
        },
        {
            "institution": "Rabobank",
            "name": "NoticeSaver",
            "base": "1.4",
            "bonus": "1.4",
            "notes": "Must give 60 days notice"
        },
        {
            "institution": "SBS",
            "name": "i-Save",
            "base": "0.05",
            "bonus": "0.05",
            "notes": ""
        },
        {
            "institution": "SBS",
            "name": "Incentive Saver",
            "base": "0.05",
            "bonus": "0.5",
            "notes": "Interest paid quarterly, must increase balance and not withdraw within that quarter to earn interest"
        },
        {
            "institution": "The Co-operative Bank",
            "name": "Step Saver",
            "base": "0.1",
            "bonus": "0.5",
            "notes": "Increase balance and no more than 1 withdrawal per month to get bonus"
        },
        {
            "institution": "The Co-operative Bank",
            "name": "Prize Draw Saver",
            "base": "0.1",
            "bonus": "0.1",
            "notes": "For each $100, one entry into draw to win car every month"
        },
        {
            "institution": "The Co-operative Bank",
            "name": "Online",
            "base": "0.1",
            "bonus": "0.1",
            "notes": ""
        },
        {
            "institution": "The Co-operative Bank",
            "name": "Dollars and Sense/Dosh",
            "base": "0.1",
            "bonus": "2.5",
            "notes": "Must be under 18 or student. Bonus paid on maximum of $4000, base on every dollar above that."
        },
        {
            "institution": "HSBC",
            "name": "Smart Saver",
            "base": "0.5",
            "bonus": "0.5",
            "notes": "$125,000 salary, $100,000 total relationship or $500,000 home loan with HSBC"
        },
        {
            "institution": "TSB",
            "name": "Horizon Savings",
            "base": "0.4",
            "bonus": "0.4",
            "notes": "$1 withdrawal fee, $350 minimum initial deposit"
        },
        {
            "institution": "TSB",
            "name": "Web Saver",
            "base": "0.4",
            "bonus": "0.4",
            "notes": "$350 minimum initial deposit, $1000 minimum balance to get interest"
        },
        {
            "institution": "Westpac",
            "name": "Simple Saver",
            "base": "0.05",
            "bonus": "0.05",
            "notes": ""
        },
        {
            "institution": "Westpac",
            "name": "Bonus Saver",
            "base": "0.05",
            "bonus": "0.25",
            "notes": "Increase balance by $20 for bonus"
        },
        {
            "institution": "Westpac",
            "name": "Notice Saver",
            "base": "0.5",
            "bonus": "0.5",
            "notes": "Must give 32 days notice"
        },
        {
            "institution": "NZCU Auckland",
            "name": "Success Saver",
            "base": "0.75",
            "bonus": "1",
            "notes": "Must be in Auckland region. Bonus rate on balances above $50,000, $3 withdrawal fee."
        },
        {
            "institution": "NZCU Auckland",
            "name": "Goal Saver",
            "base": "0.25",
            "bonus": "0.25",
            "notes": "Must be in Auckland region."
        },
        {
            "institution": "NZCU Auckland",
            "name": "Loyalty Saver",
            "base": "0.5",
            "bonus": "0.75",
            "notes": "Must be in Auckland region. Bonus rate on balances above $5,000, must give 14 days notice otherwise $20 fee"
        },
        {
            "institution": "NZCU Auckland",
            "name": "Christmas Saver",
            "base": "2",
            "bonus": "2",
            "notes": "Must be in Auckland region. Interest paid 30 Nov, must withdraw between 1 dec and 31 jan otherwise $20 fee"
        },
        {
            "institution": "NZCU Auckland",
            "name": "Kids Cash Saver",
            "base": "2",
            "bonus": "2",
            "notes": "Must be in Auckland region. Must be under 18 years old"
        },
        {
            "institution": "NZCU Baywide",
            "name": "Online Saver",
            "base": "0.35",
            "bonus": "0.55",
            "notes": "Minimum balance of $1000 for base, minimum balance of $20,000 for bonus"
        },
        {
            "institution": "NZCU Baywide",
            "name": "Everyday Saver",
            "base": "0.15",
            "bonus": "0.45",
            "notes": "Minimum balance $100,000 for bonus"
        },
        {
            "institution": "NZCU Baywide",
            "name": "Loyalty Saver",
            "base": "0.35",
            "bonus": "0.35",
            "notes": ""
        },
        {
            "institution": "NZCU Baywide",
            "name": "Christmas Saver",
            "base": "0.5",
            "bonus": "0.5",
            "notes": "Must withdraw between 1 nov to 31 jan otherwise $10 fee"
        },
        {
            "institution": "NZCU Baywide",
            "name": "Kids Saver",
            "base": "0.75",
            "bonus": "0.75",
            "notes": "10 years old and under"
        },
        {
            "institution": "First Credit Union",
            "name": "Online Saver",
            "base": "0.75",
            "bonus": "0.75",
            "notes": ""
        },
        {
            "institution": "First Credit Union",
            "name": "Loan Provider",
            "base": "1",
            "bonus": "1",
            "notes": "$20 early withdrawal fee after approval, minimum deposit $5 per month"
        },
        {
            "institution": "First Credit Union",
            "name": "Christmas Club",
            "base": "0.75",
            "bonus": "0.75",
            "notes": "Can only withdraw in Nov"
        },
        {
            "institution": "First Credit Union",
            "name": "Money Maker",
            "base": "0.25",
            "bonus": "0.75",
            "notes": "Minimum $5000 to get bonus"
        },
        {
            "institution": "First Credit Union",
            "name": "Travel",
            "base": "0.75",
            "bonus": "0.75",
            "notes": ""
        },
        {
            "institution": "Fisher and Paykel Savings & Loans",
            "name": "Everyday Saver",
            "base": "0.25",
            "bonus": "0.25",
            "notes": "Must be F&P employee"
        },
        {
            "institution": "Fisher and Paykel Savings & Loans",
            "name": "Christmas Club Saver",
            "base": "2",
            "bonus": "2",
            "notes": "Must be F&P employee, can only withdraw in Nov/Dec/Jan"
        },
        {
            "institution": "Fisher and Paykel Savings & Loans",
            "name": "Loan Provider",
            "base": "1.75",
            "bonus": "1.75",
            "notes": "Must be F&P employee, no withdrawals until after loan closed"
        },
        {
            "institution": "NZ Firefighters Credit Union",
            "name": "Christmas Club",
            "base": "1.25",
            "bonus": "1.25",
            "notes": "Must be a firefighter, can only withdraw in Nov/Dev/Jan"
        },
        {
            "institution": "NZ Firefighters Credit Union",
            "name": "Bill Pay",
            "base": "0.2",
            "bonus": "0.2",
            "notes": "Must be a firefighter"
        },
        {
            "institution": "NZ Firefighters Credit Union",
            "name": "Special Savings",
            "base": "0.5",
            "bonus": "0.5",
            "notes": "Must be a firefighter"
        },
        {
            "institution": "NZ Firefighters Credit Union",
            "name": "Primary Share",
            "base": "0.1",
            "bonus": "0.1",
            "notes": "Must be a firefighter"
        },
        {
            "institution": "NZ Firefighters Credit Union",
            "name": "Loan and Savings Provider",
            "base": "0.2",
            "bonus": "0.2",
            "notes": "Must be a firefighter"
        },
        {
            "institution": "Police Credit Union",
            "name": "Spending Account",
            "base": "0.05",
            "bonus": "0.05",
            "notes": "Must be police"
        },
        {
            "institution": "Police Credit Union",
            "name": "Achiever Saver",
            "base": "0.1",
            "bonus": "0.1",
            "notes": "Must be police, $1 minimum deposit"
        },
        {
            "institution": "Police Credit Union",
            "name": "Bonus Saver",
            "base": "0.1",
            "bonus": "0.5",
            "notes": "Must be police, no withdrawals and $20 deposit for bonus"
        },
        {
            "institution": "NZCU Steelsands",
            "name": "Goal Saver",
            "base": "0.05",
            "bonus": "0.25",
            "notes": "Minimum balance $5000 for bonus"
        },
        {
            "institution": "NZCU Steelsands",
            "name": "Loan Provider",
            "base": "0.25",
            "bonus": "0.25",
            "notes": "No withdrawals until loan closed"
        },
        {
            "institution": "NZCU Steelsands",
            "name": "Christmas Saver",
            "base": "0.25",
            "bonus": "0.8",
            "notes": "Maximum balance $5000 for bonus"
        },
        {
            "institution": "NZCU Steelsands",
            "name": "Jimmy J Junior saver",
            "base": "0.8",
            "bonus": "0.8",
            "notes": "17 years old and younger"
        },
        {
            "institution": "NZCU Employees",
            "name": "Target Saver",
            "base": "0.25",
            "bonus": "0.25",
            "notes": ""
        },
        {
            "institution": "NZCU Employees",
            "name": "Target Saver",
            "base": "0.6",
            "bonus": "0.6",
            "notes": "Can only withdraw Nov/Dev/Jan"
        },
        {
            "institution": "NZCU Employees",
            "name": "Loan Provider",
            "base": "0.25",
            "bonus": "0.25",
            "notes": "No withdrawals until loan closed"
        },
        {
            "institution": "NZCU Employees",
            "name": "Kids Club",
            "base": "0.8",
            "bonus": "0.8",
            "notes": "18 years old and younger"
        },
        {
            "institution": "Heretaunga Building Society",
            "name": "On Call",
            "base": "0.5",
            "bonus": "0.7",
            "notes": "$100,000 minimum balance for bonus, interest paid paid on 31st March and 30th September."
        },
        {
            "institution": "Nelson Building Society",
            "name": "Call",
            "base": "0.5",
            "bonus": "0.5",
            "notes": "2 free withdrawals per month, $5 per withdrawal after that"
        },
        {
            "institution": "Nelson Building Society",
            "name": "Target Saver Account",
            "base": "0.25",
            "bonus": "0.25",
            "notes": "2 free withdrawals per month, $5 per withdrawal after that"
        },
        {
            "institution": "WBS",
            "name": "Call",
            "base": "0.25",
            "bonus": "0.4",
            "notes": "$1,000 minimum balance for base, $10,000 minimum for bonus"
        },
    ]
}
setAccounts();

let desc = false;

function clearArrows() {
    document.querySelectorAll("#headings p").forEach(ele => {
        ele.textContent = ele.textContent.replace("▼", "");
        ele.textContent = ele.textContent.replace("▲", "");
    })
}

function sortBy(which) {
    setAccounts();
    accounts.sort((a, b) => {
        return a[which] - b[which];
    })
    if (!desc) {
        accounts.reverse();
        desc = true;
    } else {
        desc = false;
    }

    switch (which) {
        case "base":
            document.querySelectorAll("#headings p")[2].textContent = "Base Rate " + (desc ? "▼" : "▲");
            break;
        case "bonus":
            document.querySelectorAll("#headings p")[3].textContent = "Bonus Rate " + (desc ? "▼" : "▲");
            break;
    }

}

// Click listeners

document.querySelector("#base").addEventListener("click", (e) => {
    clearArrows();
    sortBy("base");
    createList();
});

document.querySelector("#bonus").addEventListener("click", (e) => {
    clearArrows();
    sortBy("bonus");
    createList();
});

// List Creation

function formatPercentage(percentage) {
    percentage = parseFloat(percentage).toFixed(2);
    return percentage + "% p.a.";
}

function createRow(item) {
    let newRow = document.querySelector("#row").content.cloneNode(true);

    newRow.querySelector("img").src = "/img/blog/best-savings-bank-accounts-nz/" + item.institution + ".png";
    newRow.querySelector("img").alt = item.institution;
    newRow.querySelectorAll("p")[0].textContent = item.name;
    newRow.querySelectorAll("p")[1].textContent = formatPercentage(item.base);
    newRow.querySelectorAll("p")[2].textContent = formatPercentage(item.bonus);
    newRow.querySelectorAll("p")[3].textContent = item.notes;

    document.querySelector("#table").appendChild(newRow);
}

function createList() {
    document.querySelector("#table").innerHTML = "";
    accounts.forEach(item => { createRow(item) })
}
sortBy("bonus");
createList();