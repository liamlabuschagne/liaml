document.querySelector("#burger").addEventListener("click", (e) => {
    document.querySelector("header nav").style.right = "0px";
    document.querySelector("header nav").style.display = "block";
    document.querySelector("#close").style.display = "block";
});

document.querySelector("#close").addEventListener("click", (e) => {
    document.querySelector("header nav").style.right = "-100vw";
    document.querySelector("header nav").style.display = "none";
    document.querySelector("#close").style.display = "none";
});